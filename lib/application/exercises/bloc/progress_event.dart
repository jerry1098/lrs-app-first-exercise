part of 'progress_bloc.dart';

abstract class ProgressEvent extends Equatable {
  const ProgressEvent();
}

class ProgressMadeEvent extends ProgressEvent {
  final bool answerCorrect;

  ProgressMadeEvent({@required this.answerCorrect});

  @override
  List<Object> get props => [this.answerCorrect];
}

class TotalExercisesChangedEvent extends ProgressEvent {
  final int totalExercises;

  TotalExercisesChangedEvent({@required this.totalExercises});

  @override
  List<Object> get props => [this.totalExercises];
}
