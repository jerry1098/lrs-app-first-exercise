part of 'exercise1_center_bloc.dart';

abstract class Exercise1CenterEvent extends Equatable {
  const Exercise1CenterEvent();
}

class Exercise1CenterEventShowQuestion extends Exercise1CenterEvent {
  final List<String> answers;
  final List<Color> colors;

  Exercise1CenterEventShowQuestion(
      {@required this.answers, @required this.colors});

  @override
  List<Object> get props => [this.answers, this.colors];
}

class Exercise1CenterEventShowNewQuestion extends Exercise1CenterEvent {
  final List<String> answers;
  final List<Color> colors;

  Exercise1CenterEventShowNewQuestion(
      {@required this.answers, @required this.colors});

  @override
  List<Object> get props => [this.answers, this.colors];
}
