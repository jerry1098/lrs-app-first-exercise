import 'dart:async';
import 'dart:ui';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

part 'exercise1_center_event.dart';
part 'exercise1_center_state.dart';

class Exercise1CenterBloc
    extends Bloc<Exercise1CenterEvent, Exercise1CenterState> {
  @override
  Exercise1CenterState get initialState => Exercise1CenterInitial();

  @override
  Stream<Exercise1CenterState> mapEventToState(
    Exercise1CenterEvent event,
  ) async* {
    if (event is Exercise1CenterEventShowQuestion) {
      yield Exercise1CenterShowQuestion(
          answers: event.answers, colors: event.colors, animate: false);
    }
    if (event is Exercise1CenterEventShowNewQuestion) {
      yield Exercise1CenterShowQuestion(
          answers: event.answers, colors: event.colors, animate: true);
    }
  }
}
