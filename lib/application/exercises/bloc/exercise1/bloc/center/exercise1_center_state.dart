part of 'exercise1_center_bloc.dart';

abstract class Exercise1CenterState extends Equatable {
  const Exercise1CenterState();
}

class Exercise1CenterInitial extends Exercise1CenterState {
  @override
  List<Object> get props => [];
}

class Exercise1CenterShowQuestion extends Exercise1CenterState {
  final List<String> answers;
  final List<Color> colors;
  final bool animate;

  Exercise1CenterShowQuestion(
      {@required this.answers, @required this.colors, @required this.animate});

  @override
  List<Object> get props => [this.answers, this.colors, this.animate];
}
