import 'dart:math';

import 'package:audioplayers/audio_cache.dart';

import 'Exercise1Question.dart';

class DummyBackend {
  AudioCache player;

  DummyBackend() {
    player = AudioCache(prefix: 'sounds/');
  }

  Future<Exercise1Question> getRandomQuestion() async {
    Exercise1Question question1 = Exercise1Question(
        audioFileName: 'b.mp3',
        help:
            'Drücke auf das Symbol, um einen Buchstaben zu hören. Entscheide dann welchen Buchstaben du gehört hast, indem du auf eine der drei Karten drückst.',
        possibleAnswers: ['B b', 'Sch sch', 'P p'],
        rightAnswer: 0);
    Exercise1Question question2 = Exercise1Question(
        audioFileName: 'eu.mp3',
        help:
            'Drücke auf das Symbol, um einen Buchstaben zu hören. Entscheide dann welchen Buchstaben du gehört hast, indem du auf eine der drei Karten drückst.',
        possibleAnswers: ['Au au', 'Ei ei', 'Eu eu'],
        rightAnswer: 2);
    Exercise1Question question3 = Exercise1Question(
        audioFileName: 'ü.mp3',
        help:
            'Drücke auf das Symbol, um einen Buchstaben zu hören. Entscheide dann welchen Buchstaben du gehört hast, indem du auf eine der drei Karten drückst.',
        possibleAnswers: ['Ä ä', 'Ü ü', 'Ö ö'],
        rightAnswer: 1);

    Random random = Random();
    int i = random.nextInt(3);

    if (i == 0) return question1;
    if (i == 1) return question2;
    return question3;
  }

  Future<void> playSound(String path) async {
    player.play(path);
  }
}
