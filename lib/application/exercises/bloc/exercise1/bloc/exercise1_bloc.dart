import 'dart:async';
import 'dart:ui';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:lrs_app_first_tasks/application/exercises/bloc/exercise1/bloc/body/exercise1_body_bloc.dart';
import 'package:lrs_app_first_tasks/application/exercises/bloc/exercise1/bloc/center/exercise1_center_bloc.dart';
import 'package:lrs_app_first_tasks/application/exercises/bloc/exercise1/bloc/dummyBackend.dart';
import 'package:lrs_app_first_tasks/application/exercises/bloc/progress_bloc.dart';
import 'package:meta/meta.dart';

import 'Exercise1Question.dart';

part 'exercise1_event.dart';
part 'exercise1_state.dart';

class Exercise1Bloc extends Bloc<Exercise1Event, Exercise1State> {
  ProgressBloc progressBloc;
  Exercise1CenterBloc centerBloc;
  Exercise1BodyBloc bodyBloc;

  DummyBackend dummyBackend;
  Exercise1Question currentQuestion;

  Exercise1Bloc(
      {@required this.progressBloc,
      @required this.centerBloc,
      @required this.bodyBloc,
      @required this.dummyBackend});

  @override
  Exercise1State get initialState => Exercise1Initial();

  @override
  Stream<Exercise1State> mapEventToState(
    Exercise1Event event,
  ) async* {
    if (event is Exercise1EventInitializeQuestion) {
      currentQuestion = await dummyBackend.getRandomQuestion();
      List<Color> colors = List();
      for (int i = 0; i < 3; i++) colors.add(Colors.white);
      bodyBloc.add(Exercise1BodyEventStandard());
      centerBloc.add(Exercise1CenterEventShowQuestion(
          answers: currentQuestion.possibleAnswers, colors: colors));
    }
    if (event is Exercise1EventPlaySound) {
      dummyBackend.playSound(currentQuestion.audioFileName);
    }
    if (event is Exercise1EventShowHelp) {
      bodyBloc.add(Exercise1BodyEventShowHelp(help: currentQuestion.help));
    }
    if (event is Exercise1EventHelpDismissed) {
      bodyBloc.add(Exercise1BodyEventDismissHelp());
    }
    if (event is Exercise1EventShowAbortDialog) {
      bodyBloc.add(Exercise1BodyEventShowAbort());
    }
    if (event is Exercise1EventAbortDismissed) {
      bodyBloc.add(Exercise1BodyEventDismissAbort());
    }
    if (event is Exercise1EventAbort) {
      yield Exercise1Abort();
    }
    if (event is Exercise1EventAnswerSelected) {
      int selectedAnswer = event.selectedAnswer;
      progressBloc.add(ProgressMadeEvent(
          answerCorrect: (selectedAnswer == currentQuestion.rightAnswer)));
      List<Color> colors = List();
      for (int i = 0; i < 3; i++) {
        colors
            .add(i == currentQuestion.rightAnswer ? Colors.green : Colors.red);
      }
      centerBloc.add(Exercise1CenterEventShowQuestion(
          answers: currentQuestion.possibleAnswers, colors: colors));
      await Future.delayed(Duration(milliseconds: 1500));
      currentQuestion = await dummyBackend.getRandomQuestion();
      colors.clear();
      for (int i = 0; i < 3; i++) colors.add(Colors.white);
      centerBloc.add(Exercise1CenterEventShowNewQuestion(
          answers: currentQuestion.possibleAnswers, colors: colors));
    }
  }
}
