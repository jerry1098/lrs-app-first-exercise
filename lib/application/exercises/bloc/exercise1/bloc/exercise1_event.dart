part of 'exercise1_bloc.dart';

abstract class Exercise1Event extends Equatable {
  const Exercise1Event();

  @override
  List<Object> get props => [];
}

class Exercise1EventInitializeQuestion extends Exercise1Event {}

class Exercise1EventAnswerSelected extends Exercise1Event {
  final int selectedAnswer;

  Exercise1EventAnswerSelected({@required this.selectedAnswer});

  @override
  List<Object> get props => [this.selectedAnswer];
}

class Exercise1EventPlaySound extends Exercise1Event {}

class Exercise1EventShowHelp extends Exercise1Event {}

class Exercise1EventHelpDismissed extends Exercise1Event {}

class Exercise1EventShowAbortDialog extends Exercise1Event {}

class Exercise1EventAbort extends Exercise1Event {}

class Exercise1EventAbortDismissed extends Exercise1Event {}
