part of 'exercise1_bloc.dart';

abstract class Exercise1State extends Equatable {
  const Exercise1State();

  @override
  List<Object> get props => [];
}

class Exercise1Initial extends Exercise1State {
  @override
  List<Object> get props => [];
}

class Exercise1ShowHelp extends Exercise1State {
  final String help;

  Exercise1ShowHelp({@required this.help});

  @override
  List<Object> get props => [this.help];
}

class Exercise1ShowAbortDialog extends Exercise1State {}

class Exercise1Abort extends Exercise1State {}
