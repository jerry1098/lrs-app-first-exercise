import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

part 'exercise1_body_event.dart';
part 'exercise1_body_state.dart';

class Exercise1BodyBloc extends Bloc<Exercise1BodyEvent, Exercise1BodyState> {
  @override
  Exercise1BodyState get initialState => Exercise1BodyInitial();

  @override
  Stream<Exercise1BodyState> mapEventToState(
    Exercise1BodyEvent event,
  ) async* {
    if (event is Exercise1BodyEventStandard) {
      yield Exercise1BodyStateCurrent(
          showHelp: false, showAbort: false, help: '');
    }
    if (event is Exercise1BodyEventDismissAbort) {
      yield Exercise1BodyStateCurrent(
          showHelp: false, showAbort: false, help: '');
    }
    if (event is Exercise1BodyEventDismissHelp) {
      yield Exercise1BodyStateCurrent(
          showHelp: false, showAbort: false, help: '');
    }
    if (event is Exercise1BodyEventShowAbort) {
      yield Exercise1BodyStateCurrent(
          showHelp: false, showAbort: true, help: '');
    }
    if (event is Exercise1BodyEventShowHelp) {
      yield Exercise1BodyStateCurrent(
          showHelp: true, showAbort: false, help: event.help);
    }
  }
}
