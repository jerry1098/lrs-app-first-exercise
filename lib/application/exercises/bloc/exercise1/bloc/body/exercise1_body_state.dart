part of 'exercise1_body_bloc.dart';

abstract class Exercise1BodyState extends Equatable {
  const Exercise1BodyState();
}

class Exercise1BodyInitial extends Exercise1BodyState {
  @override
  List<Object> get props => [];
}

class Exercise1BodyStateCurrent extends Exercise1BodyState {
  final bool showHelp;
  final bool showAbort;
  final String help;

  Exercise1BodyStateCurrent(
      {@required this.showHelp, @required this.showAbort, this.help});

  @override
  List<Object> get props => [this.showAbort, this.showHelp, this.help];
}
