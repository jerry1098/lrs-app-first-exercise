part of 'exercise1_body_bloc.dart';

abstract class Exercise1BodyEvent extends Equatable {
  const Exercise1BodyEvent();

  @override
  List<Object> get props => [];
}

class Exercise1BodyEventStandard extends Exercise1BodyEvent {}

class Exercise1BodyEventShowHelp extends Exercise1BodyEvent {
  final String help;
  Exercise1BodyEventShowHelp({this.help});

  @override
  List<Object> get props => [help];
}

class Exercise1BodyEventDismissHelp extends Exercise1BodyEvent {}

class Exercise1BodyEventShowAbort extends Exercise1BodyEvent {}

class Exercise1BodyEventDismissAbort extends Exercise1BodyEvent {}
