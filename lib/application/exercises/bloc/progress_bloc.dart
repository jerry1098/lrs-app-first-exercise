import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

part 'progress_event.dart';
part 'progress_state.dart';

class ProgressBloc extends Bloc<ProgressEvent, ProgressState> {
  int totalExercises;
  List<bool> alreadyDone;

  ProgressBloc() {
    alreadyDone = List<bool>();
    totalExercises = 1;
  }

  @override
  ProgressState get initialState => ProgressInitial();

  @override
  Stream<ProgressState> mapEventToState(
    ProgressEvent event,
  ) async* {
    if (event is ProgressMadeEvent) {
      alreadyDone.add(event.answerCorrect);
      yield CurrentProgressState(
          alreadyDone: alreadyDone,
          totalExercises: totalExercises,
          animate: true);
    }
    if (event is TotalExercisesChangedEvent) {
      this.totalExercises = event.totalExercises;
      yield CurrentProgressState(
          alreadyDone: alreadyDone,
          totalExercises: totalExercises,
          animate: false);
    }
  }
}
