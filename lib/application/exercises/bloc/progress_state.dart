part of 'progress_bloc.dart';

// Equatable breaking repainting for unkown reason

abstract class ProgressState {
  const ProgressState();
}

class ProgressInitial extends ProgressState {}

class CurrentProgressState extends ProgressState {
  final List<bool> alreadyDone;
  final int totalExercises;
  final bool animate;

  CurrentProgressState(
      {@required this.alreadyDone,
      @required this.totalExercises,
      @required this.animate});
}
