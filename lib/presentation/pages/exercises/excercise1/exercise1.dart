import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lrs_app_first_tasks/application/exercises/bloc/exercise1/bloc/body/exercise1_body_bloc.dart';
import 'package:lrs_app_first_tasks/application/exercises/bloc/exercise1/bloc/center/exercise1_center_bloc.dart';
import 'package:lrs_app_first_tasks/application/exercises/bloc/exercise1/bloc/dummyBackend.dart';
import 'package:lrs_app_first_tasks/application/exercises/bloc/exercise1/bloc/exercise1_bloc.dart';
import 'package:lrs_app_first_tasks/application/exercises/bloc/progress_bloc.dart';
import 'package:lrs_app_first_tasks/presentation/pages/exercises/excercise1/widgets/mainPage.dart';

class Exercise1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    DummyBackend dummyBackend = DummyBackend();
    return BlocProvider(
      create: (context) => ProgressBloc(),
      child: BlocProvider(
        create: (context) => Exercise1BodyBloc(),
        child: BlocProvider(
          create: (context) => Exercise1CenterBloc(),
          child: BlocProvider(
            create: (context) => Exercise1Bloc(
                progressBloc: BlocProvider.of<ProgressBloc>(context),
                bodyBloc: BlocProvider.of<Exercise1BodyBloc>(context),
                centerBloc: BlocProvider.of<Exercise1CenterBloc>(context),
                dummyBackend: dummyBackend),
            child: Exercise1MainPage(),
          ),
        ),
      ),
    );
  }
}
