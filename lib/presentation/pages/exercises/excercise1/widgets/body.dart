import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lrs_app_first_tasks/application/exercises/bloc/exercise1/bloc/body/exercise1_body_bloc.dart';
import 'package:lrs_app_first_tasks/application/exercises/bloc/exercise1/bloc/exercise1_bloc.dart';
import 'package:lrs_app_first_tasks/application/exercises/bloc/progress_bloc.dart';
import 'package:lrs_app_first_tasks/presentation/pages/exercises/excercise1/widgets/centerPart.dart';

class Exercise1Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<Exercise1BodyBloc, Exercise1BodyState>(
      builder: (context, state) {
        if (state is Exercise1BodyStateCurrent) {
          return Exercise1BodyWidget(
            showAbort: state.showAbort,
            showHelp: state.showHelp,
            help: state.help,
          );
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }
}

class Exercise1BodyWidget extends StatefulWidget {
  final bool showAbort;
  final bool showHelp;
  final String help;
  const Exercise1BodyWidget({this.showAbort, this.showHelp, this.help});

  @override
  State<StatefulWidget> createState() {
    return Exercise1BodyWidgetState();
  }
}

class Exercise1BodyWidgetState extends State<Exercise1BodyWidget>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  void didUpdateWidget(Exercise1BodyWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    print('Help: ' + widget.showHelp.toString());
    print('Abort: ' + widget.showAbort.toString());
    return Container(
      color: Colors.white10,
      child: SizedBox.expand(
        child: Stack(
          children: <Widget>[
            Container(
              child: SizedBox.expand(
                child: CustomPaint(
                  painter: CustomCloudPainter(),
                ),
              ),
            ),
            Align(
              child: Container(
                child: progressBarPlaceHolder(), //ProgressBar placeholder
                height: 40,
              ),
              alignment: Alignment.topCenter,
            ),
            Align(
              child: bottomButtons(false),
              alignment: Alignment.bottomRight,
            ),
            Align(
              child: bottomButtons(true),
              alignment: Alignment.bottomLeft,
            ),
            Align(
              child: Exercise1CenterPart(),
              alignment: Alignment.center,
            ),
            showHelp(widget.showHelp),
            showAbort(widget.showAbort),
          ],
        ),
      ),
    );
  }

  Widget progressBarPlaceHolder() {
    return BlocBuilder<ProgressBloc, ProgressState>(builder: (context, state) {
      if (state is CurrentProgressState) {
        return Text('Schon gemacht: ' +
            state.alreadyDone.toString() +
            ' insgesamt: ' +
            state.totalExercises.toString());
      }
      return Container();
    });
  }

  Widget showHelp(bool show) {
    double height = show ? 380 : 0;
    double width = show ? 380 : 0;
    double opacity = show ? 1.0 : 0.0;
    double opacity2 = show ? 0.5 : 0.0;
    print(show);

    return Stack(
      children: <Widget>[
        show
            ? SizedBox.expand(
                child: Container(
                child: AnimatedOpacity(
                  duration: Duration(milliseconds: 300),
                  child: Container(
                    color: Colors.black,
                  ),
                  opacity: opacity2,
                ),
              ))
            : Container(
                height: 0.0,
                width: 0.0,
              ),
        show
            ? GestureDetector(
                onTap: () {
                  BlocProvider.of<Exercise1Bloc>(context)
                      .add(Exercise1EventHelpDismissed());
                },
              )
            : Container(
                width: 0.0,
                height: 0.0,
              ),
        Align(
          alignment: Alignment.bottomRight,
          child: Padding(
            padding: EdgeInsets.only(bottom: 40.0, right: 12.0),
            child: AnimatedContainer(
              duration: Duration(milliseconds: 300),
              height: height,
              width: width,
              child: GestureDetector(
                onTap: () {
                  BlocProvider.of<Exercise1Bloc>(context)
                      .add(Exercise1EventHelpDismissed());
                },
                child: AnimatedOpacity(
                    duration: Duration(milliseconds: 300),
                    opacity: opacity,
                    child: Stack(children: <Widget>[
                      SizedBox.expand(
                        child: CustomPaint(
                          painter: CustomHelpPainter(color: Colors.white),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Column(children: <Widget>[
                          Text(
                            widget.help,
                            textAlign: TextAlign.center,
                            style: GoogleFonts.reemKufi(fontSize: 26.0),
                          ),
                        ]),
                      )
                    ])),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget showAbort(bool show) {
    return show
        ? SizedBox.expand(
            child: Stack(
              children: <Widget>[
                SizedBox.expand(
                    child: Container(
                  color: Colors.black.withOpacity(0.4),
                )),
                Center(
                  child: Container(
                    height: 150,
                    width: 300,
                    child: Card(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Willst du wirklich aufgeben?',
                            textAlign: TextAlign.center,
                            style: GoogleFonts.reemKufi(fontSize: 24.0),
                          ),
                          Container(
                            height: 10.0,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              RaisedButton(
                                color: Colors.greenAccent,
                                child: Text(
                                  'Natürlich nicht',
                                  style: GoogleFonts.reemKufi(fontSize: 16.0),
                                ),
                                onPressed: () {
                                  BlocProvider.of<Exercise1Bloc>(context)
                                      .add(Exercise1EventAbortDismissed());
                                },
                              ),
                              RaisedButton(
                                color: Colors.redAccent,
                                child: Text(
                                  'Leider schon',
                                  style: GoogleFonts.reemKufi(fontSize: 16.0),
                                ),
                                onPressed: () {
                                  BlocProvider.of<Exercise1Bloc>(context)
                                      .add(Exercise1EventAbort());
                                },
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        : Container(
            height: 0.0,
            width: 0.0,
          );
  }

  Widget bottomButtons(bool close) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: 55,
        height: 55,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
                blurRadius: 1.0,
                spreadRadius: 0.5,
                offset: Offset(0.0, 2.0),
                color: Colors.black12)
          ],
          borderRadius: BorderRadius.circular(8.0),
          color: close
              ? Color.fromRGBO(225, 80, 80, 1)
              : Color.fromRGBO(73, 220, 174, 1),
        ),
        child: InkWell(
          onTap: () {
            BlocProvider.of<Exercise1Bloc>(context).add(close
                ? Exercise1EventShowAbortDialog()
                : Exercise1EventShowHelp());
          },
          child: Center(
            child: Text(
              close ? 'X' : '?',
              style: GoogleFonts.reemKufi(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 40.0,
                  letterSpacing: 0.1),
            ),
          ),
        ),
      ),
    );
  }
}

class CustomHelpPainter extends CustomPainter {
  Color color;

  CustomHelpPainter({this.color});

  @override
  void paint(Canvas canvas, Size size) {
    Paint speechBubblePaint = Paint()
      ..color = color
      ..strokeWidth = 1.0
      ..style = PaintingStyle.fill;

    Path speechBubble = Path();
    speechBubble.moveTo(0.0, size.height * 0.5);
    speechBubble.lineTo(0.0, size.height * 0.1);
    speechBubble.quadraticBezierTo(0.0, 0.0, size.width * 0.1, 0.0);
    speechBubble.lineTo(size.width * 0.9, 0.0);
    speechBubble.quadraticBezierTo(
        size.width, 0.0, size.width, size.height * 0.1);
    speechBubble.lineTo(size.width, size.height * 0.8);
    speechBubble.quadraticBezierTo(
        size.width, size.height * 0.9, size.width * 0.9, size.height * 0.9);
    speechBubble.lineTo(size.width * 0.75, size.height * 0.9);
    speechBubble.lineTo(size.width * 0.8, size.height);
    speechBubble.lineTo(size.width * 0.65, size.height * 0.9);
    speechBubble.lineTo(size.width * 0.1, size.height * 0.9);
    speechBubble.quadraticBezierTo(
        0.0, size.height * 0.9, 0.0, size.height * 0.8);

    canvas.drawPath(speechBubble, speechBubblePaint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class CustomCloudPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint cloudPaintDarkerSky = Paint()
      ..color = Color.fromRGBO(213, 241, 254, 1)
      ..style = PaintingStyle.fill
      ..strokeWidth = 1.0;

    Paint cloudPaintLighterSky = Paint()
      ..color = Color.fromRGBO(225, 245, 254, 1)
      ..style = PaintingStyle.fill
      ..strokeWidth = 1.0;

    Paint cloudPaintdark = Paint()
      ..strokeWidth = 1.0
      ..style = PaintingStyle.fill
      ..color = Color.fromRGBO(166, 223, 249, 1);

    Rect background = Rect.fromLTRB(0.0, 0.0, size.width, size.height);
    canvas.drawRect(background, cloudPaintdark);

    Path darkerSky = Path();
    darkerSky.moveTo(0.0, 0.0);
    darkerSky.lineTo(size.width, 0.0);
    darkerSky.lineTo(size.width, size.height * 0.14);

    darkerSky.quadraticBezierTo(size.width * 0.85, size.height * 0.21,
        size.width * 0.7, size.height * 0.16);
    darkerSky.quadraticBezierTo(size.width * 0.655, size.height * 0.21,
        size.width * 0.58, size.height * 0.17);
    darkerSky.quadraticBezierTo(size.width * 0.42, size.height * 0.24,
        size.width * 0.26, size.height * 0.17);
    darkerSky.quadraticBezierTo(size.width * 0.19, size.height * 0.20,
        size.width * 0.12, size.height * 0.16);
    darkerSky.quadraticBezierTo(
        size.width * 0.05, size.height * 0.19, 0.0, size.height * 0.16);

    darkerSky.lineTo(0.0, 0.0);
    canvas.drawPath(darkerSky, cloudPaintDarkerSky);

    Path lighterSky = Path();
    lighterSky.moveTo(0.0, 0.0);
    lighterSky.lineTo(size.width, 0.0);
    lighterSky.lineTo(size.width, size.height * 0.10);

    lighterSky.quadraticBezierTo(size.width * 0.8, size.height * 0.18,
        size.width * 0.6, size.height * 0.08);
    lighterSky.quadraticBezierTo(size.width * 0.4, size.height * 0.18,
        size.width * 0.2, size.height * 0.08);
    lighterSky.quadraticBezierTo(
        size.width * 0.1, size.height * 0.13, 0.0, size.height * 0.11);

    lighterSky.lineTo(0.0, 0.0);

    canvas.drawPath(lighterSky, cloudPaintLighterSky);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
