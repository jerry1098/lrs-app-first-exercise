import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lrs_app_first_tasks/application/exercises/bloc/exercise1/bloc/exercise1_bloc.dart';
import 'package:lrs_app_first_tasks/application/exercises/bloc/progress_bloc.dart';
import 'package:lrs_app_first_tasks/presentation/pages/exercises/excercise1/widgets/body.dart';
import 'package:lrs_app_first_tasks/presentation/sharedWidgets/appBar.dart';

class Exercise1MainPage extends StatefulWidget {
  Exercise1MainPage({Key key}) : super(key: key);

  @override
  _Exercise1MainPageState createState() => _Exercise1MainPageState();
}

class _Exercise1MainPageState extends State<Exercise1MainPage> {
  ProgressBloc _progressBloc;
  Exercise1Bloc _exercise1bloc;

  @override
  void initState() {
    _progressBloc = BlocProvider.of<ProgressBloc>(context);
    _exercise1bloc = BlocProvider.of<Exercise1Bloc>(context);
    _progressBloc.add(TotalExercisesChangedEvent(totalExercises: 10));
    _exercise1bloc.add(Exercise1EventInitializeQuestion());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.blue,
        child: SafeArea(
            child: Scaffold(
                body: BlocListener<Exercise1Bloc, Exercise1State>(
          listener: (context, state) {
            if (state is Exercise1Abort) {
              Navigator.pop(context);
            }
          },
          child: BlocBuilder<Exercise1Bloc, Exercise1State>(
            builder: (context, state) {
              return Exercise1Body();
            },
          ),
        ))));
  }
}
