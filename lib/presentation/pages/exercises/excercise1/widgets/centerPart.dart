import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lrs_app_first_tasks/application/exercises/bloc/exercise1/bloc/center/exercise1_center_bloc.dart';
import 'package:lrs_app_first_tasks/application/exercises/bloc/exercise1/bloc/exercise1_bloc.dart';

class Exercise1CenterPart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<Exercise1CenterBloc, Exercise1CenterState>(
      builder: (context, state) {
        if (state is Exercise1CenterShowQuestion) {
          return Exercise1CenterPartWidget(
            answers: state.answers,
            colors: state.colors,
            animate: state.animate,
          );
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }
}

class Exercise1CenterPartWidget extends StatefulWidget {
  final List<String> answers;
  final List<Color> colors;
  final bool animate;

  Exercise1CenterPartWidget({this.answers, this.colors, this.animate, Key key})
      : super(key: key);

  @override
  _Exercise1CenterPartWidgetState createState() =>
      _Exercise1CenterPartWidgetState();
}

class _Exercise1CenterPartWidgetState extends State<Exercise1CenterPartWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: 15.0,
          ),
          Text(
            'Welchen Buchstaben\n hörst du?',
            textAlign: TextAlign.center,
            style: GoogleFonts.reemKufi(
              fontSize: 35.0,
              fontWeight: FontWeight.w500,
            ),
          ),
          Container(
            height: 0.0,
          ),
          Card(
            color: Color.fromRGBO(166, 223, 249, 1),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(100.0)),
            elevation: 0.0,
            child: InkWell(
              borderRadius: BorderRadius.circular(100.0),
              onTap: () {
                BlocProvider.of<Exercise1Bloc>(context)
                    .add(Exercise1EventPlaySound());
              },
              child: SizedBox(
                height: 155,
                width: 155,
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      top: 4.0,
                      right: 10.0,
                      child: Icon(
                        Icons.volume_up,
                        size: 145,
                        color: Colors.black26,
                      ),
                    ),
                    Icon(
                      Icons.volume_up,
                      color: Color.fromRGBO(34, 125, 184, 1),
                      size: 145,
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            height: 30.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              getAnswerButton(widget.answers[0], () {
                BlocProvider.of<Exercise1Bloc>(context)
                    .add(Exercise1EventAnswerSelected(selectedAnswer: 0));
              }, widget.colors[0]),
              getAnswerButton(widget.answers[1], () {
                BlocProvider.of<Exercise1Bloc>(context)
                    .add(Exercise1EventAnswerSelected(selectedAnswer: 1));
              }, widget.colors[1]),
              getAnswerButton(widget.answers[2], () {
                BlocProvider.of<Exercise1Bloc>(context)
                    .add(Exercise1EventAnswerSelected(selectedAnswer: 2));
              }, widget.colors[2])
            ],
          )
        ],
      ),
    );
  }

  Widget getAnswerButton(String text, Function onPressed, Color color) {
    return Card(
      elevation: 5.0,
      color: color,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      child: InkWell(
        onTap: onPressed,
        borderRadius: BorderRadius.circular(10.0),
        child: Container(
          height: 84,
          width: 84,
          child: Center(
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: GoogleFonts.reemKufi(fontSize: 32.0, height: 1.2),
            ),
          ),
        ),
      ),
    );
  }
}
