import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lrs_app_first_tasks/presentation/pages/exercises/excercise1/exercise1.dart';

class HomePageWidget extends StatelessWidget {
  const HomePageWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('HomePage'),
      ),
      body: Center(
        child: Column(children: <Widget>[
          RaisedButton(
              child: Text('Exercise1'),
              onPressed: () {
                Navigator.push(
                  context,
                  CupertinoPageRoute(builder: (context) => Exercise1()),
                );
              }),
        ]),
      ),
    );
  }
}
