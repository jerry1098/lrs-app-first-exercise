import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lrs_app_first_tasks/application/exercises/bloc/progress_bloc.dart';

class CustomProgressBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProgressBloc, ProgressState>(
      builder: (context, state) {
        if (state is CurrentProgressState) {
          return _StarPainterProgressBar();
        }
        return LinearProgressIndicator();
      },
    );
  }
}

class _StarPainterProgressBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          color: Colors.black26,
          height: 40,
          width: 40,
        ),
        Container(
            height: 60,
            width: 60,
            child: Icon(
              Icons.star,
              color: Color.fromRGBO(252, 203, 19, 1),
              size: 40,
            )),
        CustomPaint(
          painter: CustomStarPainter(),
        )
      ],
    );
  }
}

class CustomStarPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    print(size);

    Paint paint = Paint()
      ..color = Colors.amber
      ..strokeWidth = 1.0
      ..style = PaintingStyle.fill;

    Paint paint2 = Paint()
      ..color = Colors.black26
      ..strokeWidth = 3.0
      ..style = PaintingStyle.stroke;

    Path star = Path();

    /*star.lineTo(size.width * 0.5, size.height);
    star.lineTo(size.width * 0.35, size.height * 0.4);
    star.lineTo(0.0, size.height * 0.4);
    star.lineTo(size.width * 0.25, size.height * 0.55);
    star.lineTo(size.width * 0.1, size.height * 0.8);
    star.lineTo(size.width * 0.5, size.height * 0.65);
    star.lineTo(size.width * 0.9, size.height * 0.8);
    star.lineTo(size.width * 0.75, size.height * 0.55);
    star.lineTo(size.width, size.height * 0.4);
    star.lineTo(size.width * 0.65, size.height * 0.4);
    star.lineTo(size.width * 0.5, size.height);*/

    canvas.drawPath(star, paint);

    Rect rect = Rect.fromLTRB(0.0, 0.0, size.width, size.height);
    canvas.drawRect(rect, paint2);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

// old stuff
class _CustomProgressBarFromValue extends StatefulWidget {
  final List<bool> alreadyDone;
  final int totalExercises;
  _CustomProgressBarFromValue({this.alreadyDone, this.totalExercises});

  @override
  State<StatefulWidget> createState() {
    return _CustomProgressBarFromValueState();
  }
}

class _CustomProgressBarFromValueState
    extends State<_CustomProgressBarFromValue> with TickerProviderStateMixin {
  double successRatioFinal = 0.0;

  @override
  Widget build(BuildContext context) {
    Animation _animation;
    AnimationController _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 520));
    List<bool> alreadyDone = this.widget.alreadyDone;
    int totalExercises = this.widget.totalExercises;
    if (alreadyDone.length == 0)
      return Container(
        color: Colors.red,
      );
    int lastElementIndex = alreadyDone.length - 1;
    print(alreadyDone);

    // Different ratios needed to calc animation
    double singleBoxRatio = 1.0 / totalExercises;
    double oldRatio = singleBoxRatio * (alreadyDone.length - 2);
    if (oldRatio < 0) oldRatio = 0.0;
    double successRatioEnd;
    List<List<double>> failedRatios;

    if (this.widget.alreadyDone[lastElementIndex]) {
      successRatioEnd = singleBoxRatio * alreadyDone.length;
      _animation =
          Tween(begin: (successRatioEnd - singleBoxRatio), end: successRatioEnd)
              .animate(_controller)
                ..addListener(() {
                  setState(() {
                    successRatioFinal = _animation.value;
                  });
                });
      _controller.forward();
    }
    return Container(
      child: Text(successRatioFinal.toString()),
    );
  }
}

class CustomProgressBarPainter extends CustomPainter {
  List<bool> alreadyDone;
  int totalExercises;

  CustomProgressBarPainter(
      {@required this.alreadyDone, @required this.totalExercises});

  Paint toDoColor = Paint()
    ..color = Colors.black26
    ..style = PaintingStyle.fill
    ..strokeWidth = 1.0;

  Paint failedColor = Paint()
    ..color = Colors.red
    ..style = PaintingStyle.fill
    ..strokeWidth = 1.0;

  Paint successColor = Paint()
    ..color = Colors.green
    ..style = PaintingStyle.fill
    ..strokeWidth = 1.0;

  @override
  void paint(Canvas canvas, Size size) {
    double singelExerciseWidth = (1 / totalExercises) * size.width;

    Rect baseRect =
        Rect.fromPoints(Offset(0, 0), Offset(size.width, size.height));
    RRect baseRRect = RRect.fromRectAndRadius(baseRect, Radius.circular(10.0));

    Rect successRect = Rect.fromPoints(
        Offset(0, 0),
        Offset(
            (alreadyDone.length / totalExercises) * size.width, size.height));
    RRect successRRect =
        RRect.fromRectAndRadius(successRect, Radius.circular(10.0));

    canvas.drawRRect(baseRRect, toDoColor);
    canvas.drawRRect(successRRect, successColor);

    for (int i = 0; i < alreadyDone.length; i++) {
      if (!alreadyDone[i]) {
        Rect failedRect = Rect.fromPoints(Offset(singelExerciseWidth * i, 0),
            Offset(singelExerciseWidth * (i + 1), size.height));
        bool roundLeft = i == 0 || alreadyDone[i - 1];
        bool roundRight = i == alreadyDone.length - 1 || alreadyDone[i + 1];
        Radius topRight = Radius.circular(roundRight ? 10.0 : 0.0);
        Radius bottomRight = Radius.circular(roundRight ? 10.0 : 0.0);
        Radius topLeft = Radius.circular(roundLeft ? 10.0 : 0.0);
        Radius bottomLeft = Radius.circular(roundLeft ? 10.0 : 0.0);
        RRect failedRRect = RRect.fromRectAndCorners(failedRect,
            topRight: topRight,
            topLeft: topLeft,
            bottomLeft: bottomLeft,
            bottomRight: bottomRight);
        canvas.drawRRect(failedRRect, failedColor);
      }
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return oldDelegate != this;
  }
}
