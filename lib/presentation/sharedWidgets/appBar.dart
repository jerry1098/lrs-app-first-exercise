import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lrs_app_first_tasks/application/exercises/bloc/progress_bloc.dart';

class CustomAppBarWithProgressIndicator extends StatelessWidget
    implements PreferredSizeWidget {
  final double height = 56.0;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      title: Container(
        height: height,
        width: 250,
        color: Colors.red,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            BlocBuilder<ProgressBloc, ProgressState>(
              builder: (context, state) {
                if (state is CurrentProgressState) {
                  return Container(color: Colors.green, child: Text('Test'));
                }
                if (state is ProgressInitial) {
                  return LinearProgressIndicator();
                }
                return Text('Impossible');
              },
            ),
          ],
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(height);
}

class AppBarPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    print(size);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
